
const tabs = document.querySelectorAll(".service_item");
const tabsDescription = document.querySelectorAll(".description_list");

function tabClickHandler(event) {
    tabs.forEach(element => {
        element.classList.remove("active");
    });
    tabsDescription.forEach(element => {
        element.classList.remove("active");
    });

    event.target.classList.add("active");

    const descriptionsID = event.target.getAttribute("data-tab-id");
    document.getElementById(descriptionsID).classList.add("active");
}

tabs.forEach(element => {
    element.addEventListener("click", tabClickHandler)
})


// LOADER ICONS

const btn = document.querySelector(".load");
const loaderIcon = document.querySelector(".loader_icon"); 

function loaderButton () {
    let hidenImg = document.querySelectorAll(".btn_add");
    let arr = Array.prototype.slice.call(hidenImg);

    btn.style.display = "none";
    loaderIcon.style.display = "block";

    if(arr.length !== 0) {
        setTimeout(() => {
            let show = arr.splice(0, 12);
            show.forEach((el) => {
                el.classList.remove("btn_add");
            });

            loaderIcon.style.display = "none";
            btn.style.display = hidenImg.length === 0 ? "none" : "flex";
        }, 2000)

    }
}

btn.addEventListener("click", loaderButton);


// WORK FILTER

const menuItem = document.querySelectorAll(".menu_item");


function filterItem(event) {
    let all = document.querySelectorAll(".all");
    let filterClass = event.target.dataset[`filter`];
    menuItem.forEach((el) => {
        el.classList.remove("active_filter")
    })

    all.forEach((el) => {
        if(!el.classList.contains(filterClass)) {
            el.classList.add("hiden");
        }else {
            el.classList.remove("hiden");
        }
    });
    event.target.classList.add("active_filter")
};

menuItem.forEach((elem) => {
    elem.addEventListener("click", filterItem);
});


// FEEDBACK SLIDER

const prevBtn = document.querySelector(".prev_btn");
const nextBtn = document.querySelector(".next_btn");

let icons = document.querySelectorAll(".icon");
let desciptionsIcons = document.querySelectorAll(".feedback_description_item");

let i = 0;


prevBtn.addEventListener("click", () => {
    icons.forEach((el) => {
        el.classList.remove("active_feedback")
    });
    desciptionsIcons.forEach((el) => {
        el.classList.remove("active_feedback_description")
    })

    i = i - 1;

    if(i < 0) {
        i = icons.length - 1;
    }

    icons[i].classList.add("active_feedback");
    desciptionsIcons[i].classList.add("active_feedback_description");
})

nextBtn.addEventListener("click", () => {
    icons.forEach((el) => {
        el.classList.remove("active_feedback")
    });
    desciptionsIcons.forEach((el) => {
        el.classList.remove("active_feedback_description")
    })

    i++;

    if(i >= icons.length) {
        i = 0
    }

    icons[i].classList.add("active_feedback");
    desciptionsIcons[i].classList.add("active_feedback_description");
})

function iconClicker(event) {
    icons.forEach(element => {
        element.classList.remove("active_feedback");
    });
    desciptionsIcons.forEach(element => {
        element.classList.remove("active_feedback_description");
    });

    event.target.closest("li").classList.add("active_feedback");

    const descriptionsID = event.target.getAttribute("data-feedback_icon");
    document.getElementById(descriptionsID).classList.add("active_feedback_description");
}

icons.forEach(element => {
    element.addEventListener("click", iconClicker)
})
